<?php
    $server_name = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "kampus";

    $connection = mysqli_connect($server_name,$db_username,$db_password,$db_name);
    $dbconfig = mysqli_select_db($connection,$db_name);    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
    integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <title>Data</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">SIAKAD</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="mahasiswa.php">Mahasiswa</a>
      </li>
	 <li class="nav-item">
        <a class="nav-link" href="prodi.php">Prodi</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>

   
    <br><h3 class="text-center">Data Prodi</h3>
    <table class="m-auto content-table">
        <?php
            $query2 = "select * from prodi";
            $query_run2 = mysqli_query($connection, $query2);
        ?>
        <thead>
            <tr>
            <th>ID Prodi</th>
            <th>Nama Prodi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(mysqli_num_rows($query_run2) > 0)
                {
                    while($row2 = mysqli_fetch_assoc($query_run2))
                    {
                        ?>
            <tr>
                <td width="150"><?php echo $row2['id_prodi']; ?></td>
                <td width="200"><?php echo $row2['nama_prodi']; ?></td>
            </tr>
            <?php
                    }
                }
                
                else
                {
                    echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
                }
                ?>
        </tbody>
    </table>

    <br>

</body>
</html>
